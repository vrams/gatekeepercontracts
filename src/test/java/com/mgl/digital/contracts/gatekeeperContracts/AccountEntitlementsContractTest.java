package com.mgl.digital.contracts.gatekeeperContracts;


import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by vthaduri on 4/10/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccountEntitlements.class)
public abstract class AccountEntitlementsContractTest {

    @Before
    public void setUp()
    {
        RestAssuredMockMvc.standaloneSetup(new AccountEntitlements());
    }

}
