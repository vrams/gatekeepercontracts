package AccountEntitlements

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url ('/access-management/v1/entitlements')
        {
            queryParameters {
                parameter('principal_id', 12345678)
            }
        }
    }
    response {
        status 200
        body([
                "entitlements":
                        [
                                [
                                        "actions":
                                                [
                                                        "VIEW"
                                                ],
                                        "principal_id" : fromRequest().query("principal_id"),
                                        "resource_id"  : $(regex('.*?'))
                                ],
                                [
                                        "actions":
                                                [
                                                        "VIEW"
                                                ],
                                        "principal_id" : fromRequest().query("principal_id"),
                                        "resource_id"  : $(regex('.*?'))
                                ],
                                [
                                        "actions":
                                                [
                                                        "VIEW"
                                                ],
                                        "principal_id" : fromRequest().query("principal_id"),
                                        "resource_id"  : $(regex('.*?'))
                                ],
                                        [
                                            "actions":
                                                    [
                                                            "VIEW"
                                                    ],
                                        "principal_id" : fromRequest().query("principal_id"),
                                        "resource_id"  : $(regex('.*?'))
                                ],
                                [
                                        "actions":
                                                [
                                                        "VIEW"
                                                ],
                                        "principal_id" : fromRequest().query("principal_id"),
                                        "resource_id"  : $(regex('.*?'))
                                ]
                        ],
                 "errors":
                         [
                         ]
        ])
        headers {
            header('Content-Type', 'application/json;charset=UTF-8')
        }
    }
}