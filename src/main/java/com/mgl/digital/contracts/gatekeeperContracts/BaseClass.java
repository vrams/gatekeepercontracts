package com.mgl.digital.contracts.gatekeeperContracts;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Created by vthaduri on 14/10/2017.
 */
public abstract class BaseClass {

    private static final Logger logger = LoggerFactory.getLogger(BaseClass.class);

    private PropertiesConfiguration propertiesConfiguration;
    public String ENVIRONMENT = "";
    public void setEnvironment()
    {
        try
        {
            propertiesConfiguration = new PropertiesConfiguration("application.properties");
        }
        catch (ConfigurationException e)
        {
            e.printStackTrace();
        }
        if(System.getProperty("url") == null | System.getProperty("env") == null)
        {
            ENVIRONMENT = propertiesConfiguration.getString("url.dev");
        }
        else if(System.getProperty("url") != null)
        {
            ENVIRONMENT = System.getProperty("url");
        }
        else if(System.getProperty("env") != null)
        {
            ENVIRONMENT = propertiesConfiguration.getString(System.getProperty("env"));
        }
    }

    public ResponseEntity<String> getResponse(String url)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        logger.info("JSON Response: " + responseEntity.toString());
        return responseEntity;
    }

}
