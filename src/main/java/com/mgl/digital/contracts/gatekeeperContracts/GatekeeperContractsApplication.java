package com.mgl.digital.contracts.gatekeeperContracts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatekeeperContractsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatekeeperContractsApplication.class, args);
	}
}
