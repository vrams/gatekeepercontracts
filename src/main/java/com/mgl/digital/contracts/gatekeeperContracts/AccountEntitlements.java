package com.mgl.digital.contracts.gatekeeperContracts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by vthaduri on 3/10/2017.
 */
@RestController
public class AccountEntitlements extends BaseClass
{
    private static final Logger logger = LoggerFactory.getLogger(AccountEntitlements.class);
    private final String ACCOUNT_REQUEST = "/access-management/v1/entitlements";

    @RequestMapping(value = ACCOUNT_REQUEST)
    public ResponseEntity<String> getEntitlements(@RequestParam("principal_id") String principal_id)
    {
        setEnvironment();
        String url = ENVIRONMENT + ACCOUNT_REQUEST + "?principal_id=" + principal_id;
        return getResponse(url);
    }

}
